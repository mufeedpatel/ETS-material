1998��8�� ��������

1. M: I don't think you have time to send out the invitations to all the students.
W: Oh, yes. I will.
Q: What do we learn about the woman from this conversation?  (A)

2. W: Nobody told me that Bill was in the hospital.
M: Sorry. I meant to give you a call when I found out but it slipped my mind.
Q: What does the man mean?  (D)

3. M: I don't know if I will be able to turn in my economics paper on time.
W: Have you heard that the professor gave us a week of extension on it.
Q: What does the woman mean?  (B)

4. M: I'd like to make an appointment with the doctor for tomorrow.
W: Unfortunately he is completely booked.
Q: What does the woman mean?  (C)

5. M: Joe just went down to the engineering meeting.
W: Where is it?
Q: What does the woman want to know?  (A)

6. M: I have a collect call from Mike Peterson.
W: I will accept the charges.
Q: What does the woman mean?  (B)

7. W: I'd like really to go to the concert tonight, but I don't know if I could spare the time.
M: Music always relaxing me. It might be worth it in a long run.
Q: What does the man suggest the woman do?  (C)

8. M: Those airplanes are certainly loud.
W: Aren't they though?
Q: What does the woman think of the airplanes?  (A)

9. M: Helen and I are thinking of renting a house at the beach in June. Are you interested?
W: You? I guess it is cheaper then, but do you really think it will be warm enough?
Q: What does the woman mean?  (D)

10. M: I'm getting hungry. I think we should go to the dinner soon.
W: Me too. All I have for lunch was a chocolate bar.
Q: What does the man mean?   (C)

11. M: Your apartment always looks so good. So spotless and mine is such a mess.
W: I've been at the lab the all week. It is my roommate doing.
Q: What does the woman imply?  (A)

12. M: I am running out of coins during my laundry.
W: That's too bad.
Q: What does the woman mean?  (C)

13. W: It's a shame that you didn't win your tennis match.
M: I might have won if I listened to my coach.
Q: What does the man imply?  (C)

14. M: The Variety Theatre finally went out the business.
W: Well, that's not a surprise. It was the worst one in town.
Q: What does the woman mean?  (B)

15. W: Shall we run around the park or go for a bike ride?
M: It makes no difference to me. They are both good activities.
Q: What does the man say about the activities.  (D)

16. M: I can't to the life to get that washing machine downstairs to work. Do you have any suggestions?
W: Try washing just half of the normal load.
Q: What does the woman suggest the man do?  (A)

17. M: I'm think of dropping my swimming class. I am just not catching on.
W: Stick with it. I did and I learned how to swim eventually.
Q: What can be inferred about the woman?  (C)

18. M: Doctor, this cough medicine doesn't seem to helping. Can you give me a different prescription?
W: Let's give another day or two to see how you are doing then.
Q: What does the doctor imply?  (D)

19. W: Would you like to see those pants in another color? They are also coming in brown and in Navy.
M: Actually the gray is fine but I prefer something in wool.
Q: What will the woman probably do next?  (D)

20. W: Professor Burns seems to think that there is only one way to write paper and that's her way.
M: No kidding, she sure wasn't like that the last semester.
Q: What can be inferred about professor Burns?  (B)

21. W: This Barbecue sure beats the last one we went to Ha?
M: Oh that's right. Everyone had to spend the whole time inside. The good thing is the weather decided to cooperate this time around.
Q: What can be inferred from this conversation?  (D)

22. M: That new position requires a letter of reference. I guess the one the professor wrote for me last year should be fine. Don't you think?
W: It is a little dated though. You might need to submit a current one.
Q: What does the woman suggest the man do?  (A)

23. W: I don't think I want to be on the curriculum committee anymore but I'm not sure how to get out of it .
M: Well, you know there are plenty of people who will be interested. Me, for example.
Q: What does the man imply?  (C)

24. W: Excuse me could you do for me the Customer Service? I need to have this gift wrapped.
M: We can take care of that right here man, and no charge. You can choose either silver or gold with the matching bowl.
Q: What will the woman probably do the next?  (B)

25. M: These plants next to the window always look brown. You wouldn't know by looking at them that I have the watered them every week.
W: Maybe they don't like the direct sunlight. I have the same problem of some of my plants and little shade could help them immensely.
Q: What does the woman suggest the man do?  (A)

26. W: Oh no I just picked up the pictures I took at Dan and Linda's wedding and looked at them and none of them came out.
M: They are dark, aren't they? What a shame. Well I'm sure the professional photographer got everything.
Q: What does What does the man mean?  (A)

27. M: I get a feeling that Sally never really listens to me.
W: You said it. As it is that she is always using the time to rehearse what she will say next.
Q: What does the woman imply?  (D)

28. W: Will you make sure all the members of student advisory committee know what to expect at the tomorrow's meeting?
M: They will have a briefing this afternoon.
Q: What does the man mean?  (D)

29. W: How do you like my new poster. It was only twenty dollars.
M: Really? The frame alone is worth the money.
Q: What does the man mean?  (D)

30. M: I hear Mary isn't getting much support in her running against Steve in the election.
W: It is not over yet. I think she will make a come back.
Q: What does the woman mean?  (A)

Question 31-34. Listen to a conversation between two friends.
M: I have been studying too much and need a change. So I just making plans to go away during January break.
W: Really? Where are you going?
M: I'm planning to visit New Mexico.
W: My sister and I had the vacation there last year and we had a great time.
M: Did you get into Albuquerque?
W: Sure. Whenever we were skating.
M: Is it far from the mountains?
W: Not at all. There are even though Albuquerque on the high plateaus. There are even higher mountains near it. Just half an hour away from the city there is a snow-covered slope.
M: Well. As the mountains are just thirty minutes away, I guess I should take my ice skate and my ski's.
W: Definitely.
M: I heard that the weather there is great.
W: It is. No humidity, moderate temperatures, but you do need to be careful about high altitude.
M: What should I do about that?
W: Oh, just take it easy for a few days. Don't go hiking up to the mountains or exercise too vigorously. Just do everything gradually.
M: I'm sure I will be fine. And I will let know all about my trip when I come back.
31. What's the main purpose of the man's trip?  (B)
32. Why does the woman know so much about Albuquerque?  (D)
33. What can be inferred about the man?  (C)
34. According to the woman, what may cause the man the most problems in the Albuquerque?  (C)

Question 35-38. Listen to two students talking about eating in the school cafeteria.
M: Hey Linda, do you get that letter about the new options for food service next year?
W: Not yet. Are there a lot of changes?
M: There sure are. Instead of paying one fee to cover all meals for the whole school year, we are now be able to choose by seven, ten, fourteen or twenty-one meals per week. They give you a card with the number of meals you get for a week marked on it.
W: That's a big change Tom. And a complicated system.
M: Yeah. But it will be much better for people who don't eat three meals a day, seven days a week in the cafeteria because they don't have to pay meals they don't eat.
W: So what's the deal for those who do eat at school all the time?
M: It's better for them too. Because the meal you contract, the cheaper each one is.
W: I see. It is still sound rather complicated.
M: True. It took me several hours to figure it out. I decided to go with the ten Meals.
W: Why is that?
M: Well, I never eat breakfast and I often go away on weekends. So the ten meal plan gives me lunch and dinner each weekday at a fairly low price. And I won't be paying for meals I don't usually eat.
W: And what about the weekend when you are on campus?
M: Well, there are often guests on campus at weekends. So they allow you to buy single meals on a walk-in basis on Saturdays and Sundays. The price per meal is much higher in that way. But I an away so much that it will still be less money for me to pay single prices on the weekends rather than sign up for the fourteen meal a week plan.
W: Oh, I guess I'll have to sit down and figure out my eating pattern so I can get the best deal.
35. What's the main feature of the new method of paying for meals?  (B)
36. When do the students pay for the meals they contract for?  (C)
37. How does the new plan benefit the students who eat all their meals at the school cafeteria?  (D)
38. How can weekend guest eat at the cafeteria?  (A)

Questions 39 to 42. Listen to this talk being given to college campus.
I was really glad when your club invited me to share my coin collection. It's been my passion since I collected my first Lincoln dime in 1971. That is the current coin with Abraham's image. Just a little history before I started my own collection. Lincoln pennies are made of copper and they were the first the United States coins to bear the lightness of the president. It was minted in 1909 when the country was celebration the centennial of Lincoln's birth than 1809 that the decision was made to redesign the one-cent piece in his honor. Before that, the penny has an American Indian head on it. The new penny was designed by artist Victor David Braner. It is interesting because he put his initials DVB on the reverse of the coin ad the original design. There was a general abort when the initial was discovered. And only a limited numbers of coins were strutted with the initials on them. Today a penny with the initials from a San Francisco mint called the 1909s' DVB was worth 500 dollars. Now  when I started my coin collection, I began with penny for several reasons. There were a lot of them, several hundred billion were minted and there were a lot of people collecting them. So I have plenty of people to trade with and talk to about my collection. Also it was the coin I could afford to collect as a young teenager. In the twenty-five years since then, I have managed to acquire over three hundred coins; some of them are very rare. I will be sharing with you today some of my rare specimen including the 1909s' DVB.

39. Why does the woman collect coins?  (C)
40. Why were letters DVB on pennies?  (D)
41. What was one of reasons the collector collected coins as a teenager?  (B)
42. What will the speaker do next?  (B)

Question 43 to 46. Listen to talk from a biology class. 
Today I want to talk to you about the wasps and their nests. You recall the biologist divided species of wasps into two groups solitary and social. Solitary wasps as the name implied do not live together with other wasps. In most species the male and female get together only to mate and then the female does all the work of building the nest and providing the food for the offspring by herself. Solitary wasps usually make nest on the ground and they separate the chambers for the individual offspring with the grass, stone or mud, whatever is handy. What about social wasps? They form a community and work together to build and maintain the nest. A nest begins in the spring when the fertile female called the queen build the first new compartment in the nest and lay eggs. The first offspring are females but cannot lay eggs. These females called workers. They build a lot of new compartments and the queen lays more eggs. They also care for the new offspring and defend the nest with their stingers. By the way only the female have stingers. Most social wasps make nest of paper. The female produces the paper by chewing out fibers or old wood. They spread the papers in thin layers to make cells, which the queen lays her eggs. Most of you I'm sure have seen these nests suspended from the trees. They may also be built on the ground in abundant road bowls.

43. Who builds the nest of solitary wasps?  (C)
44. Why the female wasps are more dangers to people than the male wasps are?  (D)
45. What is the main function of the queen ?  (B)
46. What are the nests of social wasps made of?  (C)

Question 47 to 50. Listen to a talk in class about United States history.
What was the most popular mix about the United States in the 19th Century was that of the free and simple life of the farmer. It was said that the farmers worked hard on their own land to produce whatever their families' needed. They might sometimes trade with their neighbors, but in general they could get along just fine by relying on themselves, not on commercial ties with others. This is how Thomas Jefferson idealized the farmers at the beginning of the 19th century. And at that time, this may have been close to the truth especially on the frontier. But by the mid century sweeping changes in agriculture were well under way as farmers began to specialized in the raising of crops such as cotton or corn or wheat. By late in the century revolutionary invents in farm machinery has vastly increased the production of specialized crops and extensive network of railroads had linked farmers throughout the country to market in the east and even overseas. By raising and selling specialized crops, farmers could afford more and finer goods and achieved much higher standard of living but at a price. Now farmers were no longer dependent just on the weather and their own efforts, their lives were increasing controlled by the banks, which had powder to grant or deny loans for new machinery, and by the railroads which set the rates for shipping their crops to the market. As businessmen, farmers now had to worry about national economic depression and the implement of world supply and demand on for example, the of price of wheat in hands. And so by the end of the 19th century, the era of Jefferson's independent farmer had come to a close.

47. What is the main topic of the talk?  (D)
48. According to the professor, what was the major change in the agriculture during the 19th century?  (A)
49. According to the professor, what was one result of the increased use of machinery on farms of the United States?  (B)
50. According to the professor, why was world market important for the United States agriculture?  (C)
